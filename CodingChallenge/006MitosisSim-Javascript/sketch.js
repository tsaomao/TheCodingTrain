// Described as simple interactive primitive
// Bunch of floating circles on the screen.
// Click on one and it splits into two.
var cells = [];

function setup() {
  createCanvas(800, 800);
  var cell = new Cell();
  var cell2 = new Cell();
  cells.push(cell);
  cells.push(cell2);
}

function draw() {
  // gray
  background(51);
  for (var i = 0; i < cells.length; i++) {
    cells[i].move();
    cells[i].grow();
    cells[i].show();
  }
}

function mousePressed() {
  var mousepos = createVector(mouseX, mouseY);
  // iterating forwards while modifying the array can cause, essentially,
  // skipping and race conditions. So instead, we'll iterate backwards, so that any operations
  // we do on the end of the list are already passed by.
  for (var i = cells.length - 1; i >= 0; i--) {
    if (cells[i].clicked(mousepos.x, mousepos.y)) {
      var parent = cells[i];
      cells.splice(i, 1);
      var mitarr = parent.mitosis();
      cells = cells.concat(mitarr);
    }
  }
}
