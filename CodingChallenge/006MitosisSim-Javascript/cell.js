function Cell(posx, posy, r_, c_) {
  this.alpha = 200;
  if (typeof(posx)==='undefined' || typeof(posy)==='undefined') {
    this.pos = createVector(random(width), random(height));
  } else {
    this.pos = createVector(posx, posy);
  }
  this.r = r_ || 80;
  this.c = c_ || color(random(50, 255), 0, random(50, 255), this.alpha);
  this.maxr = 80;
  this.growthrate = 0.25;
  this.alpha = 0.80;

  this.clicked = function(x, y) {
    var d = dist(this.pos.x, this.pos.y, x, y);

    if (d < this.r/2) {
      return true;
    } else {
      return false;
    }
  }

  this.grow = function() {
    if (this.r < this.maxr) {
      this.r = this.r + this.growthrate;
    }
  }

  this.mitosis = function() {
    var cellA = new Cell(this.pos.x + this.r/2, this.pos.y, this.r/2, this.c);
    var cellB = new Cell(this.pos.x - this.r/2, this.pos.y, this.r/2, this.c);
    var retarr = [];
    retarr.push(cellA);
    retarr.push(cellB);
    // Add these returned elements to cells array in sketch.js with array.concat() function.
    return retarr;
  }

  this.move = function() {
    var vel = p5.Vector.random2D();
    this.pos.add(vel);
  }

  this.show = function() {
    // purple
    fill(this.c);
    noStroke();
    ellipse(this.pos.x, this.pos.y, this.r, this.r);
  }
}
