class Planet {
  float radius;
  float angle;
  float distance;
  Planet[] planets;
  float orbitSpeed;
  PVector v;
  
  PShape globe;
  
  Planet(float r_, float d_, float o_, PImage img) {
    v = PVector.random3D();
    
    radius = r_;
    distance = d_;
    v.mult(distance);
    angle = random(2*PI);
    orbitSpeed = o_;
    
    noStroke();
    noFill();
    globe = createShape(SPHERE, radius);
    globe.setTexture(img);
  }
  
  void spawnMoons(int total, int level) {
    planets = new Planet[total];
    for (int i = 0; i < planets.length; i++) {
      float r = radius/(level*2);
      float d = random((radius + r), ((radius + 2) * 2));
      float o = random(-0.1, 0.1);
      int index = int(random(0, textures.length));
      planets[i] = new Planet(r, d, o, textures[index]);
      if (level < 2) {
        int num = int(random(0, 3));
        planets[i].spawnMoons(num, level+1);
      }
    }
  }
  
  void orbit() {
    angle = angle + orbitSpeed;
    if (planets != null) {
      for (int i = 0; i < planets.length; i++) {
        planets[i].orbit();
      }
    }
  }
  
  void show() {
    pushMatrix();
    noStroke();
    //Obtain a perpendicular vector
    PVector v2 = new PVector(1, 0, 1);
    PVector p = v.cross(v2);
    rotate(angle, p.x, p.y, p.z);
    
    // Try to see the vectors
    //stroke(255);
    //line(0,0,0, v.x, v.y, v.z);
    //line(0,0,0, p.x, p.y, p.z);
    
    translate(v.x, v.y, v.z);
    //noStroke();
    fill(255);
    //sphere(radius);
    shape(globe);
    if (planets != null) {
      for (int i = 0; i < planets.length; i++) {
        planets[i].show();
      }
    }
    popMatrix();
  }
}
