import peasy.*;

Planet sun;

PImage sunTexture;
PImage[] textures = new PImage[3]; 

PeasyCam cam;

void setup() {
  size(800, 800, P3D);
  sunTexture = loadImage("data/sun.jpg");
  textures[0] = loadImage("mars.jpg");
  textures[1] = loadImage("earth.jpg");
  textures[2] = loadImage("mercury.jpg");
  cam = new PeasyCam(this, 500);
  sun = new Planet(50, 0, 0, sunTexture);
  sun.spawnMoons(4, 1);
}

void draw() {
  background(0);
  //lights();
  ambientLight(64, 64, 64);
  directionalLight(128, 128, 128, 0, 0, -1);
  directionalLight(128, 128, 128, 0, 0, 1);
  directionalLight(128, 128, 128, 0, -1, 0);
  directionalLight(128, 128, 128, 0, 1, 0);
  directionalLight(128, 128, 128, -1, 0, 0);
  directionalLight(128, 128, 128, 1, 0, 0);

  lightFalloff(1, 0, 0);
  lightSpecular(0, 0, 0);
  pointLight(255, 255, 255, 0, 0, 0);
  sun.show();
  sun.orbit();
}
