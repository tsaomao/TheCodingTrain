function Planet(r_, d_, o_) {
  this.planets = [];

  this.radius = r_;
  this.distance = d_;
  this.angle = random(2*PI);
  this.orbitSpeed = o_;

  this.spawnMoons = function(total, level) {
    for (let i = 0; i < total; i++) {
      var r = this.radius/(level*2);
      var d = random(50, 150)/level;
      var o = random(-0.1, 0.1);
      this.planets.push(new Planet(r, d, o));
      if (level < 2) {
        var num = Math.floor(random(0, 3));
        this.planets[i].spawnMoons(num, level+1);
      }
    }
  }

  this.orbit = function() {
    this.angle = this.angle + this.orbitSpeed;
    for (let i in this.planets) {
        this.planets[i].orbit();
    }
  }

  this.show = function() {
    push();
    rotate(this.angle);
    translate(this.distance, 0);
    fill(255, 100);
    ellipse(0, 0, this.radius*2, this.radius*2);
    for (let i in this.planets) {
      this.planets[i].show();
    }
    pop();
  }
}
