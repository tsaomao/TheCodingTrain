// hold the snake in s
var s;
// scl is used to make a grid of spaces where the box goes
var scl = 20;

// hold the food in food
var food;

// Note the frameRate() call that slows down the rendering a little
function setup() {
  createCanvas(600,600);
  s = new Snake();
  frameRate(10);
  // pickLocation() places the food randomly according to the grid
  pickLocation();
}

// Use floor() for every calculation here to keep the grid griddy
function pickLocation() {
  var cols = floor(width/scl);
  var rows = floor(height/scl);
  food = createVector(floor(random(cols)), floor(random(rows)));
  food.mult(scl);
}

// Debug: click to lengthen the snake
function mousePressed() {
  s.total++;
}

// s.eat() checks proximity to food and returns true or false If true, move it.
function draw() {
  background(51);

  if (s.eat(food)) {
    pickLocation();
  }
  s.death();
  s.update();
  s.show();


  fill(255, 0, 100);
  rect(food.x, food.y, scl, scl);
}

// Capture keystrokes for arrow keys and respond appropriately
// By setting the Snake's xspeed and yspeed with Snake.dir()
function keyPressed() {
  if (keyCode === UP_ARROW) {
    s.dir(0, -1);
  } else if (keyCode === DOWN_ARROW) {
    s.dir(0, 1);
  } else if (keyCode === RIGHT_ARROW) {
    s.dir(1, 0);
  } else if (keyCode === LEFT_ARROW) {
    s.dir(-1, 0);
  }
}
