function Cell(i, j) {
  //column
  this.i = i;
  //row
  this.j = j
  //top, right, bottom, left
  this.walls = [true, true, true, true];

  this.show = function(w_) {
    //fill(51);
    //stroke(255);
    var x = this.i * w_;
    var y = this.j * w_;
    //rect(x, y, w_, w_);
    stroke(255);
    if (this.walls[0]) {
      //Top Line
      line(x+w_, y   , x   , y   );
    }
    if (this.walls[1]) {
      //Right Line
      line(x+w_, y+w_, x+w_, y   );
    }
    if (this.walls[2]) {
      //Bottom Line
      line(x   , y+w_, x+w_, y+w_);
    }
    if (this.walls[3]) {
      //Left Line
      line(x   , y   , x   , y+w_);
    }
  }
}
