// Malcolm Gin
// from Daniel Shiffman @ The Coding Train
// Depth-first search
// Recursive backgracker
// https://en.wikipedia.org/wiki/Maze_generation_algorithm#Depth-first_search

var rows, cols;
var w = 80;
// NOTE: 1D Array
var cells = [];

function setup() {
  createCanvas(800, 800);
  cols = floor(width / w);
  rows = floor(height / w);

  // To me, cols are x/i and rows are y/j.
  for (let j = 0; j < rows; j++) {
    for (let i = 0; i < cols; i++) {
      var cell = new Cell(i, j);
      cells.push(cell);
    }
  }
}

function draw() {
  background(51);
  for (let i = 0; i < cells.length; i++) {
    cells[i].show(w);
  }
}
