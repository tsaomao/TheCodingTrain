var shp;
var flwrs = [];
var drps = [];
var flowersperrow = 7;
var flowerspacing = 20;
var flowerwidth = 60;
var flowerheaderspace = 60;
var rowwidth = 0;
var margin = 0;

function setup() {
  createCanvas(800, 800);
  shp = new Ship();

  // First figure outwhere to start the row, so it's centered.
  rowwidth = (flowersperrow - 1) * (flowerwidth + flowerspacing);
  margin = (width - rowwidth)/2;

  for (var i = 0; i < flowersperrow; i++) {
    flwrs[i] = new Flower(margin+(i*(flowerwidth+flowerspacing)), flowerheaderspace);
  }
}

function draw() {
  // This works for playability better than keyPressed() for left/right on the ship
  // keyPressed() makes you tap the key every time you want to move it.
  // Putting this in draw() means you can hold it down to keep moving in that
  // direction.
  if (keyIsDown(RIGHT_ARROW)) {
    shp.move(1);
  }

  if (keyIsDown(LEFT_ARROW)) {
    shp.move(-1);
  }

  // Too many drops
  //if (keyIsDown(32)) {
  //  var drop = new Drop(shp.x, height-shp.r);
  //  drps.push(drop);
  //}

  background(51);
  shp.show();

  // Edge detection for flowers - right or left edge of canvas for row
  var edge = false;

  for (var i = 0; i < flwrs.length; i++) {
    flwrs[i].move();
    flwrs[i].show();

    if (flwrs[i].x > width || flwrs[i].x < 0) {
      edge = true;
    }
  }

  if (edge) {
    for (var i = 0; i < flwrs.length; i++) {
      flwrs[i].shiftDown();
    }
  }

  // on hit, we're going to remove the drop and make the flower grow
  for (var i = 0; i < drps.length; i++) {
    drps[i].move();
    drps[i].show();

    for (var j = 0; j < flwrs.length; j++) {
      if (drps[i].hits(flwrs[j])) {
        flwrs[j].grow();
        drps[i].evaporate();
      }
    }
  }

  // Can remove drops flagged for deletion at bottom of loop above, but
  // it's possible to skip steps if doing so by iterating forward through the array
  // so best to iterate backwards.
  for (var i = (drps.length - 1); i >= 0; i--) {
    if (drps[i].toDelete) {
      // Using splice() and not offering a third argument deletes staarting at i,
      // 1 element.
      drps.splice(i, 1);
    }
  }
}

function keyPressed() {
  if (key === ' ') {
    var drop = new Drop(shp.x, height-shp.r);
    drps.push(drop);
  }
}
