// This is the space invader object.
// We'll make an array of these.
function Flower(x, y) {
  this.x = x;
  this.y = y;
  this.r = 10;
  // Not yet used
  this.xdir = 1;

  this.show = function() {
    // purple
    fill(255, 0, 200);
    ellipse(this.x, this.y, this.r*2, this.r*2);
    // yellow
    fill(200, 200, 0);
    noStroke()
    ellipse(this.x, this.y, this.r/1.5, this.r/1.5);
  }

  this.move = function() {
    this.x = this.x + this.xdir;
  }

  this.shiftDown = function() {
    // move other direction
    this.xdir *= -1;
    this.y += this.r;
  }

  this.grow = function() {
    this.r = this.r + 2;
  }
}
