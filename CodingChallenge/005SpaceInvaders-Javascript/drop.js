// Copied originally from flower.js
// This is the water droplet the ship shoots at the flowers/invaders
function Drop(x, y) {
  this.x = x;
  this.y = y;
  this.r = 8;
  this.toDelete = false;

  this.speed = 10;

  this.show = function() {
    // blue
    fill(50, 25, 200);
    noStroke();
    // a bit of fiddling to make these overlap and look drop-like
    // probably wawnt to minimize the number of shapes associated with each Drop for performance reasons
    // but this will do for now
    ellipse(this.x, this.y-this.r*0.25, this.r*1.5, this.r*1.5);
    ellipse(this.x, this.y+this.r*0.33, this.r*0.75, this.r*0.75);
    ellipse(this.x, this.y+this.r*0.66, this.r/2, this.r/2);
    ellipse(this.x, this.y+this.r, this.r/2, this.r/2);
  }

  this.move = function() {
    this.y = this.y - 5;
  }

  this.hits = function(flwr) {
    var dstn = dist(this.x, this.y, flwr.x, flwr.y);

    // Should probably refactor for better accuracy, but it's probably okay for this
    // implementation.
    if (dstn < (this.r+flwr.r)) {
      return true;
    } else {
      return false;
    }
  }

  this.evaporate = function() {
    this.toDelete = true;
  }
}
