// This is the triangular watering can ship that waters the flower invaders
// in Dan Shiffman's concept game.
// In Dan's tutorial, he makes it a rectangle, but mentions a triangle.
function Ship() {
  this.x = width/2;
  this.speed = 10;
  this.r = 40;

  this.show = function() {
    fill(255);
    triangle(this.x-(this.r/2), height, this.x, height-this.r, this.x+(this.r/2), height);
  }

  this.move = function(direct) {
    this.x += direct * this.speed;
  }
}
