// Purple Rain
// Purple color: (138, 43, 226)
// Background: (230, 230, 250)

// Need lots of rain.
Drop[] drops = new Drop[750];

// Set up canvas 800 x 800 pixels
void setup() {
  size(800, 800);
  // Create all the drops
  for (int i = 0; i < drops.length; i++) {
    drops[i] = new Drop();
  }
}

void draw() {
  // Background color provided by internet
  background(230, 230, 250);
  for (int i = 0; i < drops.length; i++) {
    drops[i].fall();
    drops[i].show();
  }
}
