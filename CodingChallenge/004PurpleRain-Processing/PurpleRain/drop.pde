// Eacth drop as instantiation of class
class Drop {
  float x = random(width);
  float y = random(-200, height);
  // parallax for virtual distance z
  // Derivative values are mapped from z
  float z = random(0, 20);
  float len = map(z, 0, 20, 10, 20);
  float yspeed = map(z, 0, 20, 0.5, 20);
  float thick = map(z, 0, 20, 1, 4);

  void fall() {
    y = y + yspeed;
    // a gravity factor, random, accelerates drops to bottom of canvas
    float grav = map(z, 0, 20, 0, 0.2);
    yspeed = yspeed + grav;
    
    // When a raindrop hits the "ground", reposition it back to above the canvas
    if (y > height) {
      y = random(-200, -10);
      yspeed = map(z, 0, 20, 0.5, 20);
    }
  }
  
  // strokeWeight is parallaxed thickness and parallaxed length
  void show() {
    strokeWeight(thick);
    // Color provided by Internet
    stroke(138, 43, 226);
    line(x, y, x, y+len);
  }
}
