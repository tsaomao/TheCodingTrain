// Eacth drop as instantiation of class
function Drop() {
  this.x = random(width);
  this.y = random(-200, height);
  // parallax for virtual distance z
  // Derivative values are mapped from z
  this.z = random(0, 20);
  this.len = map(this.z, 0, 20, 10, 20);
  this.yspeed = map(this.z, 0, 20, 0.5, 20);
  this.thick = map(this.z, 0, 20, 1, 4);

  this.fall = function() {
    this.y = this.y + this.yspeed;
    // a gravity factor, random, accelerates drops to bottom of canvas
    var grav = map(this.z, 0, 20, 0, 0.2);
    this.yspeed = this.yspeed + grav;

    // When a raindrop hits the "ground", reposition it back to above the canvas
    if (this.y > height) {
      this.y = random(-200, -10);
      this.yspeed = map(this.z, 0, 20, 0.5, 20);
    }
  }

  // strokeWeight is parallaxed thickness and parallaxed length
  this.show = function() {
    strokeWeight(this.thick);
    // Color provided by Internet
    stroke(138, 43, 226);
    line(this.x, this.y, this.x, this.y+this.len);
  }
}
