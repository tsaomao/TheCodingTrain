// Purple Rain
// Purple color: (138, 43, 226)
// Background: (230, 230, 250)

// Need lots of rain.
var drops = [];
var numDrops = 750;

// Set up canvas 800 x 800 pixels
function setup() {
  createCanvas(800, 800);
  // Create all the drops
  for (var i = 0; i < numDrops; i++) {
    drops[i] = new Drop();
  }
}

function draw() {
  // Background color provided by internet
  background(230, 230, 250);
  for (var i = 0; i < numDrops; i++) {
    drops[i].fall();
    drops[i].show();
  }
}
