class Box {
  // Hold position and size
  PVector pos;
  float r;

  // Constructor
  Box(float x, float y, float z, float r_) {
    pos = new PVector(x, y, z);
    r = r_;
  }
  
  // Generate the next set of boxes, based on current generation
  // Return an array of boxes
  ArrayList<Box> generate() {
    ArrayList<Box> boxes = new ArrayList<Box>();
    // Iterate through the 3 axes (abstracted to i, j, k so we don't only rely on variable scoping for values)
    // The temptation to use x, y, z for these iterative variables is high, but use i, j, k, so that in case something breaks scoping
    // there's still isolation.
    // Count each from -1 to 1 (there are 3) for ease of math below in if (sum > 1) to cut out the 3d cross of boxes we want to lose
    for (int i = -1; i < 2; i++) {
      for (int j = -1; j < 2; j++) {
        for (int k = -1; k < 2; k++) {
          // Mathematical check. If sum is greater than 1 (if more than one coordinate's absolute value is 1)
          // then the current box is not in t he 3d cross we're cutting out.
          int sum = abs(i) + abs(j) + abs(k);
          // The new box's size is 1/3 of the current (larger) box
          float newR = r/3;
          // Again, only return as part of the new array if the new sub box is not part of the 3d cross
          // The pattern is (in 3 slices of the box from any normal to any face:
          // 
          //   OOO   OXO   OOO
          //   OXO   XXX   OXO
          //   OOO   OXO   OOO
          //
          if (sum > 1) {
            Box b = new Box(pos.x+i*newR, pos.y+j*newR, pos.z+k*newR, newR);   
            boxes.add(b);
          }
        }
      }
    }
    return boxes;
  }
  
  void show() {
    // Just a tiny bit of recursion.
    pushMatrix();
    translate(pos.x, pos.y, pos.z);
    box(r);
    popMatrix();
  }
  
}
