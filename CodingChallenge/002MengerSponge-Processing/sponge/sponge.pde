// A menger songe is a box with boxes cut out of it in a 3d cross, and each sub-box also has boxes cut out of it.
// This one gets more complex with each mouse click.
float a = 0;
ArrayList<Box> sponge;

void setup() {
  // Setup 800 x 800 pixel 3D canvas
  size(800, 800, P3D);
  // the sponge is an array of boxes
  sponge = new ArrayList<Box>();
  
  // The initial box is 300 pixels on a side.
  Box b = new Box(0,0,0,300);
  // Add initial box to the sponge array.
  sponge.add(b);
}

void mousePressed() {
  // On click, increase the sponginess of the sponge (add boxes not in the 3d-cross-hole.
  ArrayList<Box> next = new ArrayList<Box>();
  for (Box b: sponge) {
    // Box.generate() is what decides which boxes to add to the array
    ArrayList<Box> newBoxes = b.generate();
    next.addAll(newBoxes);
  }
  // Replace the old sponge array with the new array (next)
  sponge = next;
}

// Sponge styling
void draw() {
  // Canvas: gray
  background(51);
  // vertices: black
  stroke(0);
  // faces: white
  fill(255);
  // turn on lights for specular shading (to help see boxes and holes)
  lights();

  //draw box in center of canvas
  translate(width/2, height/2);
  // rotate on two axes, just a little, every frame, for visibility
  rotateX(a);
  rotateY(a/2);
  // Iterate through array to draw every box.
  for (Box b: sponge) {
    b.show();
  }
  
  a += 0.01;
}
