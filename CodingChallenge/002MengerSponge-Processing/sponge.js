// A menger songe is a box with boxes cut out of it in a 3d cross, and each sub-box also has boxes cut out of it.
// This one gets more complex with each mouse click.
var a = 0;
var sponge = [];

function setup() {
  // Setup 800 x 800 pixel 3D canvas
  createCanvas(800, 800, WEBGL);
  // The initial box is 300 pixels on a side.
  var b = new Box(0, 0, 0, 300);
  // Add initial box to the sponge array.
  // the sponge is an array of boxes
  sponge.push(b);
}

function mousePressed() {
  // On click, increase the sponginess of the sponge (add boxes not in the 3d-cross-hole.
  var next = [];
  for (var i = 0; i < sponge.length; i++) {
    var b = sponge[i];
    // Box.generate() is what decides which boxes to add to the array
    var newBoxes = b.generate();
    next = next.concat(newBoxes);
  }
  // Replace the old sponge array with the new array (next)
  sponge = next;
}

// Sponge styling
function draw() {
  // Canvas: gray
  background(51);
  // vertices: black
  stroke(0);
  // faces: white
  fill(255);

  // rotate on two axes, just a little, every frame, for visibility
  rotateX(a);
  rotateY(a * 0.4);
  rotateZ(a * 0.1);

  // Iterate through array to draw every box.
  for (var i = 0; i < sponge.length; i++) {
    sponge[i].show();
  }

  a += 0.01;
}
