// Array for all the stars
Star[] stars = new Star[800];

// Speed is controlled by the mouse X coordinates
float speed;

void setup() {
  // Canvas size: 800 x 800 pixels
  size(800, 800);
  // Create all the stars in the array
  for (int i = 0; i < stars.length; i++) {
    stars[i] = new Star();
  }
}

// draw() is called repeatedly to re-render the canvas
void draw() {
  speed = map(mouseX, 0, width, 0, 50);
  //black
  background(0);
  // Translate origin on canvas to center
  translate(width / 2, height / 2);
  // Cycle through stars array, update and show each one
  for (int i = 0; i < stars.length; i++) {
    stars[i].update();
    stars[i].show();
  }
}
