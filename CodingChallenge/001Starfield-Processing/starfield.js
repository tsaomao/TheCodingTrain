// Array for all the stars
var stars = [];
var numStars = 800;

// Speed is controlled by the mouse X coordinates
var speed;

function setup() {
  // Canvas size: 800 x 800 pixels
  createCanvas(800, 800);
  // Create all the stars in the array
  for (var i = 0; i < numStars; i++) {
    stars[i] = new Star();
  }
}

// draw() is called repeatedly to re-render the canvas
function draw() {
  speed = map(mouseX, 0, width, 0, 50);
  //black
  background(0);
  // Translate origin on canvas to center
  translate(width / 2, height / 2);
  // Cycle through stars array, update and show each one
  for (var i = 0; i < stars.length; i++) {
    stars[i].update();
    stars[i].show();
  }
}
