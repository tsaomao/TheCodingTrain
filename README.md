# JavaScript-and-p5

For lessons learned while following along with [The Coding Train](https://www.youtube.com/user/shiffman).

Primarily for learning using JavaScript with p5.js and matter.js.

Testable versions should live at Git-deploy-powered web hosting location [on my web host](http://www.malcolmgin.com/TheCodingTrain/). Will try to maintain a [directory.html](http://www.malcolmgin.com/TheCodingTrain/directory.html) for guiding you through it, but you may also need to infer/explore WebDAV folder structure from this repository.

See [directory.html](http://www.malcolmgin.com/TheCodingTrain/directory.html) for more links, context, and remarks.

(Also note: I migrated this repo from GitHub to GitLab in May 2018. See [directory.html](http://www.malcolmgin.com/TheCodingTrain/directory.html) for the GitHub link if you need it for some reason.)
